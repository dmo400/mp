#include <algorithm>
#include "workqueue.hpp"

WorkQueue::WorkQueue(int bufSize, int elSize)
{
    buffer = new long[bufSize * elSize];
    capacity = bufSize;
    elementSize = elSize;
    head = 0;
    tail = 0;
    closed = false;
}

WorkQueue::~WorkQueue()
{
    delete[] buffer;
}

bool WorkQueue::submit(PolGenerator *p) {
    boost::unique_lock<boost::mutex> lock(mut);
    while (is_full() && !closed) {
        not_full.wait(lock);
    }
    
    if (closed) {
        lock.unlock();
        not_empty.notify_all();
        return false;
    }
    
    p->read_avalues(buffer + (tail * elementSize));
    tail = (tail + 1) % capacity;
    
    lock.unlock();
    not_empty.notify_one();
    
    return true;
}

bool WorkQueue::retrieve(long container[]) {
    boost::unique_lock<boost::mutex> lock(mut);
    while(is_empty() && !closed) {
        not_empty.wait(lock);
    }
    
    if (is_empty() && closed) {
        lock.unlock();
        not_full.notify_all();
        return false;
    }
    
    long *element = buffer + head * elementSize;
    std::copy(element, element + elementSize, container);
    head = (head + 1) % capacity;
    
    lock.unlock();
    not_full.notify_one();
    
    return true;
}

bool WorkQueue::is_empty() {
    return head == tail;
}

bool WorkQueue::is_full() {
    int next = (tail + 1) % capacity;
    
    return next == head;
}

void WorkQueue::close() {
    closed = true;
}
