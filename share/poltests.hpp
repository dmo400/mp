#ifndef POLTESTS_HPP
#define POLTESTS_HPP

#include <vector>
#include "pari/pari.h"

long const PRECISION = DEFAULTPREC;
int const POL_DEGREE = 7;
int const POL_TERMS = 8;
int const PATTERN_SIZE = 4;

extern GEN BOUND_PART;
extern std::vector<std::vector<long>> testResults;

GEN create_polynomial(long coefficients[]);
void tests_setup();
void tests_teardown();
int test_discriminant(GEN polynomial);
int test_hunters_bound(GEN polynomial);
int test_nf_discriminant(GEN polynomial);
void test_polynomial(long coefficients[]);

#endif // POLTESTS_HPP
