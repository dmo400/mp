#include <iostream>
#include <vector>
#include "pari/pari.h"
#include "poltests.hpp"
#include "statistics.hpp"

GEN BOUND_PART;
std::vector<std::vector<long>> testResults;
long nf_pattern[PATTERN_SIZE];

/*
 * Generate a GEN polynomial from the coefficients in array c
 */
GEN create_polynomial(long c[]) {
    pari_sp ltop, lbot;
    GEN p, v;

    ltop = avma;
    v = cgetg(POL_TERMS + 1, t_VECSMALL);
    for (int i = 0; i < POL_TERMS; i++) {
        v[i + 1] = c[i];
    }
    lbot = avma;
    p = gerepile(ltop, lbot, gtopolyrev(v, -1));

    return p;
}

/*
 * Setups the necessary variables for the subsequent tests
 * Calculates B and BOUND_PART for Hunters Bound, exposes BOUND_PART outside of the PARI stack
 */
void tests_setup() {
    pari_sp ltop;
    GEN B;
    ltop = avma;
    // B the high value constant used to calculate a part in Hunters Bound
    B = mulsi(11L, powuu(10L, 5L)); // B = 1.1 * 10^6 = 11 * 10^5 = 1 100 000
    // BOUND_PART which never changes, used as part of Hunters BOUND
    BOUND_PART = gclone(mulsr(2L, powrfrac(rdivis(B, 21L, PRECISION), 1L, 6L))); // BOUND_PART = 2 * ((B / 21)^(1 / 6))
    avma = ltop;
}

/*
 * Tear down everything which was setup for the tests
 * Unclones BOUND_PART as it was not on the PARI stack
 */
void tests_teardown() {
    gunclone(BOUND_PART);
}

/*
 * Hunters Bound, requires irreducible polynomial as input
 */
int test_hunters_bound(GEN polynomial) {
    pari_sp ltop;
    GEN polRoots, rootsSum, HuntersBound;
    int isInBound;
    
    ltop = avma;
    polRoots = roots(polynomial, PRECISION); // Roots of polynomial, results in 7 roots for degree 7
    for (int i = 1; i <= POL_DEGREE; i++) {
        gel(polRoots, i) = sqrr(gabs(gel(polRoots, i), PRECISION)); // Squares each root of the PARI vector polRoots in place
    }
    rootsSum = vecsum(polRoots); // Sums the squared roots in polRoots up as rootsSum
    HuntersBound = addrr(rdivis(sqri(gel(polynomial, 2 + POL_DEGREE - 1)), 7L, PRECISION), BOUND_PART); // Hunters Bound = ((a1^2) / 7) + BOUND_PART
    isInBound = cmprr(rootsSum, HuntersBound);
    avma = ltop;
    
    return isInBound <= 0;
}

/*
 * Discriminant test
 * Polynomial discriminant != 0 && 3^a * 5^b * 7^c * r^2
 */
int test_discriminant(GEN polynomial) {
    pari_sp ltop;
    GEN discriminant, remainder;
    long exp3, exp5, exp7;
    int isSquare;
    
    ltop = avma;
    discriminant = absi(poldisc0(polynomial, -1L));
    if (equalii(discriminant, gen_0)) {
        return 0;
    }
    exp3 = Z_lval(discriminant, 3L);
    exp5 = Z_lval(discriminant, 5L);
    exp7 = Z_lval(discriminant, 7L);
    remainder = divii(discriminant, mulii(mulii(powuu(3L, exp3), powuu(5L, exp5)), powuu(7L, exp7)));
    isSquare = Z_issquare(remainder);
    avma = ltop;
    
    return isSquare;
}

int test_nf_discriminant(GEN polynomial) {
    pari_sp ltop;
    GEN nf, discriminant;
    long signdisc, exp3, exp5, exp7;
    int isEqual;
    
    ltop = avma;
    nf = nfinit(polynomial, PRECISION);
    discriminant = nf_get_disc(nf);
    signdisc = gcmpgs(discriminant, 0L);
    discriminant = absi(discriminant);
    exp3 = Z_lval(discriminant, 3L);
    exp5 = Z_lval(discriminant, 5L);
    exp7 = Z_lval(discriminant, 7L);
    isEqual = equalii(discriminant, mulii(mulii(powuu(3L, exp3), powuu(5L, exp5)), powuu(7L, exp7)));
    avma = ltop;
    
    if (isEqual) {
        nf_pattern[0] = signdisc;
        nf_pattern[1] = exp3;
        nf_pattern[2] = exp5;
        nf_pattern[3] = exp7;
    }
    
    return isEqual;
}

/*
 * Number Field Discriminant test
 */
void test_polynomial(long coefficients[]) {
    pari_sp ltop = avma;
    GEN polynomial;
    int hasResult;
    
    st_timer_record_start(ST_RUN);
    
    polynomial = create_polynomial(coefficients);
    
    st_timer_record_start(ST_DISC);
    hasResult = test_discriminant(polynomial);
    st_timer_store_elapsed(ST_DISC);
    
    if (hasResult) {
        st_timer_record_start(ST_IRRED);
        hasResult = isirreducible(polynomial);
        st_timer_store_elapsed(ST_IRRED);
    }
    
    if (hasResult) {
        st_timer_record_start(ST_HUNTER);
        hasResult = test_hunters_bound(polynomial);
        st_timer_store_elapsed(ST_HUNTER);
    }
    
    if (hasResult) {
        st_timer_record_start(ST_NFDISC);
        hasResult = test_nf_discriminant(polynomial);
        st_timer_store_elapsed(ST_NFDISC);
    }
    
    if (hasResult) {
        st_timer_record_start(ST_SAVE);
        std::vector<long> result;
        for (int i = 0; i < POL_TERMS; i++) {
            result.push_back(coefficients[i]);
        }
        for (int i = 0; i < PATTERN_SIZE; i++) {
            result.push_back(nf_pattern[i]);
        }
        testResults.push_back(result);
        st_timer_store_elapsed(ST_SAVE);
    }
    avma = ltop;
    st_timer_store_elapsed(ST_RUN);
}
