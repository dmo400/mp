#ifndef STATISTICS_HPP
#define STATISTICS_HPP

#include <chrono>

int const ST_SIZE = 11;
int const ST_TOTAL = 0;
int const ST_RECV = 1;
int const ST_GENERATE = 2;
int const ST_STORE = 3;
int const ST_RETRIEVE = 4;
int const ST_RUN = 5;
int const ST_DISC = 6;
int const ST_IRRED = 7;
int const ST_HUNTER = 8;
int const ST_NFDISC = 9;
int const ST_SAVE = 10;

void st_init();
void st_durations_reset_to(std::chrono::high_resolution_clock::duration (&durations)[ST_SIZE]);
void st_counts_reset_to(long (&counts)[ST_SIZE]);
void st_timer_record_start(int part);
void st_timer_store_elapsed(int part);
long st_microseconds_elapsed(int part);
long st_counted(int part);

#endif // STATISTICS_HPP

