#ifndef POLGENERATOR_HPP
#define POLGENERATOR_HPP

#include "pari/pari.h"

class PolGenerator
{
    GEN boundconstant; // 2 * (B / n)^(1 / (n - 1))
    GEN t2;
    long *avalues; // a_k values
    long *svalues; // s_k values
    long *sbounds; // s_k upper bounds
    long *dvalues; // d_k values until A_k reaches lower bounds
    int n; // degree of the polynomial
    int partsk; // until which k this generator will generate coefficients for
    int lockedk; // a_n values which cannot be changed where n <= k
    
    void t2_calc();
    void a_init(int k);
    bool a_next(int k);
    void s_sync(int k);
    void s_bind(int k);
    void d_calc(int k);
    
public:
    PolGenerator(GEN boundconstant, int n, int partsk);
    ~PolGenerator();
    void init();
    void load(const long a[], int asize);
    void lock(int k);
    bool configure_next();
    void read_avalues(long container[]);
    void create_coefficients(long container[]);
};

#endif // POLGENERATOR_HPP
