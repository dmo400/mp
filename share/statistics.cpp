#include <chrono>
#include "pari/pari.h"
#include "statistics.hpp"

std::chrono::time_point<std::chrono::high_resolution_clock> ST_START_TIMES[ST_SIZE];
std::chrono::high_resolution_clock::duration ST_DURATIONS[ST_SIZE];
long ST_COUNTS[ST_SIZE];

void st_init() {
    for (int i = 0; i < ST_SIZE; i++) {
        ST_DURATIONS[i] = std::chrono::high_resolution_clock::duration::zero();
        ST_COUNTS[i] = 0L;
    }
}

void st_durations_reset_to(std::chrono::high_resolution_clock::duration (&durations)[ST_SIZE]) {
    for (int i = 0; i < ST_SIZE; i++) {
        ST_DURATIONS[i] = durations[i];
    }
}

void st_counts_reset_to(long (&counts)[ST_SIZE]) {
    for (int i = 0; i < ST_SIZE; i++) {
        ST_COUNTS[i] = counts[i];
    }
}

void st_timer_record_start(int part) {
    ST_START_TIMES[part] = std::chrono::high_resolution_clock::now();
}

void st_timer_store_elapsed(int part) {
    auto time = std::chrono::high_resolution_clock::now();
    ST_DURATIONS[part] += (time - ST_START_TIMES[part]);
    ST_COUNTS[part] += 1L;
}

long st_microseconds_elapsed(int part) {
    return std::chrono::duration_cast<std::chrono::microseconds>(ST_DURATIONS[part]).count();
}

long st_counted(int part) {
    return ST_COUNTS[part];
}
