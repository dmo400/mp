#include <algorithm>
#include "polgenerator.hpp"

PolGenerator::PolGenerator(GEN boundconstant, int n, int partsk)
{
    this->boundconstant = gclone(boundconstant);
    t2 = nullptr;
    avalues = new long[partsk + 1];
    svalues = new long[partsk + 1];
    sbounds = new long[partsk + 1];
    dvalues = new long[partsk + 1];
    this->n = n;
    this->partsk = partsk;
    lock(0);
}

PolGenerator::~PolGenerator()
{
    gunclone(boundconstant);
    if (t2 != nullptr) {
        gunclone(t2);
    }
    delete[] avalues;
    delete[] svalues;
    delete[] sbounds;
    delete[] dvalues;
}

// Calculate the t2 based on the current value of a_1
void PolGenerator::t2_calc() {
    GEN t2value;
    
    if (t2 != nullptr) {
        gunclone(t2);
    }
    pari_sp ltop = avma;
    t2value = addrr(rdivis(sqri(stoi(avalues[1])), (long) n, DEFAULTPREC), boundconstant);
    t2 = gclone(t2value);
    avma = ltop;
}

// Calculate a_k based on a_n, s_n where n < k and bound of s_k
// Sets s_k by calling s_sync(k) and d_k by calling d_calc(k)
// Requires k > 0 and the bound on s_k to be set beforehand
void PolGenerator::a_init(int k) {
    pari_sp ltop;
    long s, r;
    long a = 0L;
    
    if (k == 1) {
        a = -(n / 2); // a_1 = -(n / 2)
    } else if ((k == 3) && (avalues[1] == 0L)) {
        a = 0L; // a_3 >= 0 if a_1 = 0
    } else if (k == n) {
        a = -sbounds[k]; // |a_n| <= (t2)^(n/2) / 7^(n/2)
    } else {
        if (k == 2) {
            // a_2 has a more refined lower bound than -sbound[2]
            ltop = avma;
            // (2 / n) * (a_1)^2 - t2
            s = itos(truncr(subrr(mulri(rdivis(gen_2, (long) n, DEFAULTPREC), sqri(stoi(avalues[1]))), t2)));
            avma = ltop;
        } else {
            s = -sbounds[k];
        }
        for (int i = 1; i < k; i++) {
            r = avalues[k - i] * svalues[i];
            if ((i % 2) == 0) {
                a -= r;
            } else {
                a += r;
            }
        }
        if ((k % 2) == 0) {
            a -= s;
        } else {
            a += s;
        }
        a /= k;
    }
    
    avalues[k] = a;
    s_sync(k);
    d_calc(k);
}

// Sets the next value of a_k using d_k if possible
// Returns true if a_k was changed, else false
bool PolGenerator::a_next(int k) {
    if (dvalues[k] < 1) {
        return false;
    }
    
    if ((k % 2) == 0) {
        avalues[k] -= 1L;
    } else {
        avalues[k] += 1L;
    }
    svalues[k] += (long) k;
    dvalues[k] -= 1L;
    
    return true;
}

// Calculate the s_k based on current a_n where n <= k
// Requires k > 0
void PolGenerator::s_sync(int k) {
    long s = 0L;
    long r;
    
    if ((k == 1) || (k == n)) {
        svalues[k] = avalues[k];
    } else {
        for (int i = 1; i < k; i++) {
            r = avalues[i] * svalues[k - i];
            if ((i % 2) == 0) {
                s -= r;
            } else {
                s += r;
            }
        }
        if ((k % 2) == 0) {
            s -= k * avalues[k];
        } else {
            s += k * avalues[k];
        }
        svalues[k] = s;
    }
}

// Calculate the upperbounds for s_k
// Requires t2 to be known/calculated beforehand
void PolGenerator::s_bind(int k) {
    pari_sp ltop = avma;
    if (k == 1) {
        sbounds[k] = 0L; // Always 0
    } else if (k == 2) {
        sbounds[k] = itos(floorr(t2)); // t2^(2/2) = t2, shortcut
    } else if (k == n) {
        sbounds[k] = itos(floorr(divrr(powrshalf(t2, n), powrshalf(stor(n, DEFAULTPREC), n))));
    } else {
        sbounds[k] = itos(floorr(powrshalf(t2, (long) k)));
    }
    avma = ltop;
}

// Calculates the remaining delta of how many times A_k can be lowered by 1 until reaching the lower bound
// Requires k > 0 and up to date s_k with its bound
void PolGenerator::d_calc(int k) {
    if (k == 7) {
        dvalues[k] = 2 * sbounds[k];
    } else {
        dvalues[k] = (sbounds[k] - svalues[k]) / k;
    }
}

// Initialize generator to starting values
// Sets all values based on s_bind() and a_init(), calls t2_calc() after a_1 is set
void PolGenerator::init() {
    // Always set k = 0 as monic polynomial
    avalues[0] = 1L;
    svalues[0] = 1L;
    sbounds[0] = 1L;
    dvalues[0] = 0L;
    if (n < 1) {
        return;
    }
    // Set k=1 for t2
    s_bind(1);
    a_init(1);
    t2_calc();
    for (int i = 2; i <= partsk; i++) {
        // Initialize all other values in ascending k order
        s_bind(i);
        a_init(i);
    }
}

// Load the specified a_k values from a[] where k > 0
// Requires asize > 0
void PolGenerator::load(const long a[], int asize) {
    // Always set k = 0 as monic polynomial
    avalues[0] = 1L;
    svalues[0] = 1L;
    sbounds[0] = 1L;
    dvalues[0] = 0L;
    // Copy a[] to avalues[]
    std::copy(a, a + asize, avalues + 1);
    t2_calc();
    for (int i = 1; i <= asize; i++) {
        // Sets corresponding values according to the set a_k
        s_bind(i);
        s_sync(i);
        d_calc(i);
    }
    for (int i = asize + 1; i <= partsk; i++) {
        // Initialize the leftover values
        s_bind(i);
        a_init(i);
    }
}

// Indicates the last n coefficients are not to be changed
void PolGenerator::lock(int k) {
    lockedk = k;
}

// Configure the generator from the current values to the next iteration
// Returns true if the internal state was changed
// Returns false if no unlocked value had any dvalue left
bool PolGenerator::configure_next() {
    for (int i = partsk; i > 0; i--) {
        if (i <= lockedk) {
            // Reached a locked k
            return false;
        } else if (a_next(i)) {
            // Successfully changed a_i into next value
            if (i == 1) {
                // Recalculate t2 if a_1 was changed
                t2_calc();
                for (int i = 2; i <= partsk; i++) {
                    // Recalculate every bound on s_k with the new t2
                    s_bind(i);
                }
            }
            for (int j = i + 1; j <= partsk; j++) {
                 // Initialize all a_k where k > i
                 a_init(j);
            }
            return true;
        }
    }
    
    return false; // Reached limit, no variants possible anymore
}

// Copy all avalues[] to container[] excluding a_0
void PolGenerator::read_avalues(long container[]) {
    std::copy(avalues + 1, avalues + partsk + 1, container);
}

// Transforms a_k into a polynomial according to:
// a_0x^7 - a_1x^6 + a_2x^5 - a_1x^4 + a_2x^3 - a_1x^2 + a_2x - a_7
// Coefficients are stored in ascending order, starting with the constant term
void PolGenerator::create_coefficients(long container[]) {
    std::reverse_copy(avalues, avalues + partsk + 1, container);
    for (int i = 0; i < partsk; i += 2) {
        container[i] *= -1L;
    }
}
