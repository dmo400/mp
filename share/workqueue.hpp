#ifndef WORKQUEUE_HPP
#define WORKQUEUE_HPP

#include <boost/thread.hpp>
#include "polgenerator.hpp"

class WorkQueue
{
    boost::condition_variable not_empty;
    boost::condition_variable not_full;
    boost::mutex mut;
    long *buffer;
    int capacity;
    int elementSize;
    long head;
    long tail;
    bool closed;
    
public:
    WorkQueue(int bufSize, int elSize);
    ~WorkQueue();
    bool submit(PolGenerator *p);
    bool retrieve(long container[]);
    bool is_empty();
    bool is_full();
    void close();
};

#endif // WORKQUEUE_HPP
