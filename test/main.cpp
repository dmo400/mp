#include "pari/pari.h"
#include "generatortest.hpp"
#include "polteststest.hpp"

/* PARI necessary global variables */
static long const PARI_STACK_SIZE = 1000000L;
static long const PARI_MAXPRIME = 2L;

int main(int argc, char **argv) {
    pari_init(PARI_STACK_SIZE, PARI_MAXPRIME);

    run_suite_generator_totals();

    run_suite_discriminant();
    run_suite_hunters_bound();

    pari_close();
}
