#include "generatortest.hpp"
#include "log.hpp"
#include "polgenerator.hpp"
#include "poltests.hpp"

const static long GENERATOR_TOTALS_3 = 3114L;
const static long GENERATOR_TOTALS_4 = 371238L;
const static long GENERATOR_TOTALS_5 = 138176698L;

void run_generator_totals(std::string test, long expected, int genVars) {
    long count = 0L;
    PolGenerator polGen(BOUND_PART, genVars);

    polGen.init();
    count = 1L;
    while (polGen.up_coefficient()) {
        count += 1L;
    }
    assert_long(test, expected, count);
}

void run_suite_generator_totals() {
    log_info("Start suite: Generator totals");
    tests_setup();

    run_generator_totals("Generator totals with 3 coefficients", GENERATOR_TOTALS_3, 3);
    run_generator_totals("Generator totals with 4 coefficients", GENERATOR_TOTALS_4, 4);
    run_generator_totals("Generator totals with 5 coefficients", GENERATOR_TOTALS_5, 5);

    log_info("End suite: Generator totals");
}

void run_generator_partials() {
}
