#ifndef LOG_HPP
#define LOG_HPP

#include <string>

void log_info(std::string info);
void log_pass(std::string message);
void log_fail(std::string message);
void assert_true(std::string test, bool result);
void assert_false(std::string test, bool result);
void assert_long(std::string test, long expected, long result);

#endif // LOG_HPP

