#include "pari/pari.h"
#include "log.hpp"
#include "poltests.hpp"
#include "polteststest.hpp"

GEN generate_polynomial(long const c[]) {
    pari_sp ltop, lbot;
    GEN p, v;
    
    ltop = avma;
    v = mkvecsmalln(8, c[0], c[1], c[2], c[3], c[4], c[5], c[6], c[7]);
    lbot = avma;
    p = gerepile(ltop, lbot, gtopolyrev(v, -1));
    
    return p;
}

void run_discriminant(std::string const test, bool const expected, long const (&coefficients)[POL_TERMS]) {
    pari_sp ltop;
    GEN polynomial;
    int result;

    ltop = avma;
    polynomial = generate_polynomial(coefficients);
    result = test_discriminant(polynomial);
    if (expected) {
        assert_true(test, result);
    } else {
        assert_false(test, result);
    }
    avma = ltop;
}

void run_suite_discriminant() {
    log_info("Start suite: Discriminant");
    tests_setup();

    run_discriminant("x^7 - 3*x^6 - 2*x^5 + 25*x^4 - 60*x^3 + 77*x^2 - 56*x + 21", true, {21, -56, 77, -60, 25, -2, -3, 1});
    run_discriminant("x^7 - 3*x^6 - 2*x^5 + 25*x^4 - 60*x^3 + 77*x^2 - 56*x + 22", false, {22, -56, 77, -60, 25, -2, -3, 1});
    run_discriminant("x^7 + 6*x^5 + 5*x^4 - 15*x^3 - 21*x^2 - 5*x - 6", true, {-6, -5, -21, -15, 5, 6, 0, 1});
    run_discriminant("x^7 + 6*x^5 + 5*x^4 - 15*x^3 - 21*x^2 - 5*x - 7", false, {-7, -5, -21, -15, 5, 6, 0, 1});
    run_discriminant("x^7 + 2*x^6 + 6*x^5 + 5*x^4 + 10*x^3 + 6*x^2 + 7*x - 4", true, {-4, 7, 6, 10, 5, 6, 2, 1});
    run_discriminant("x^7 + 2*x^6 + 6*x^5 + 5*x^4 + 10*x^3 + 6*x^2 + 7*x - 3", false, {-3, 7, 6, 10, 5, 6, 2, 1});

    tests_teardown();
    log_info("End suite: Discriminant");
}

void run_hunters_bound(std::string const test, bool const expected, long const (&coefficients)[POL_TERMS]) {
    pari_sp ltop;
    GEN polynomial;
    int result;

    ltop = avma;
    polynomial = generate_polynomial(coefficients);
    result = test_hunters_bound(polynomial);
    if (expected) {
        assert_true(test, result);
    } else {
        assert_false(test, result);
    }
    avma = ltop;
}

void run_suite_hunters_bound() {
    log_info("Start suite: Hunters Bound");
    tests_setup();

    // a1 = 0, bound = 174.86346730767690711784513612685550890
    run_hunters_bound("x^7 - 89*x^5 - 240*x^4 + 1024*x^3 + 3120*x^2 - 2736*x - 8639", false, {-8639, -2736, 3120, 1024, -240, -89, 0, 1}); // 178
    run_hunters_bound("x^7 - 89*x^5 - 120*x^4 + 2104*x^3 + 4800*x^2 - 7056*x - 17279", false, {-17279, -7056, 4800, 2104, -120, -89, 0, 1}); // 178
    run_hunters_bound("x^7 - 88*x^5 - 94*x^4 + 2487*x^3 + 4654*x^2 - 21360*x - 50399", false, {-50399, -21360, 4654, 2487, -94, -88, 0, 1}); // 176
    run_hunters_bound("x^7 - 84*x^5 - 126*x^4 + 1155*x^3 + 2142*x^2 - 1072*x - 2015", true, {-2015, -1072, 2142, 1155, -126, -84, 0, 1}); // 168
    run_hunters_bound("x^7 - 86*x^5 + 56*x^4 + 1753*x^3 - 1624*x^2 - 5700*x + 5601", true, {5601, -5700, -1624, 1753, 56, -86, 0, 1}); // 172
    run_hunters_bound("x^7 - 82*x^5 + 52*x^4 + 2037*x^3 - 2308*x^2 - 15300*x + 25201", true, {25201, -15300, -2308, 2037, 52, -82, 0, 1}); // 164

    // a1 = 1, bound = 175.00632445053404997498799326971265175
    run_hunters_bound("x^7 + x^6 - 91*x^5 - 245*x^4 + 434*x^3 + 1204*x^2 - 344*x - 959", false, {-959, -344, 1204, 434, -245, -91, 1, 1}); // 183
    run_hunters_bound("x^7 + x^6 - 93*x^5 - 157*x^4 + 2708*x^3 + 6036*x^2 - 23616*x - 60479", false, {-60479, -23616, 6036, 2708, -157, -93, 1, 1}); // 187
    run_hunters_bound("x^7 + x^6 - 89*x^5 - 173*x^4 + 2128*x^3 + 4852*x^2 - 13200*x - 28799", false, {-28799, -13200, 4852, 2128, -173, -89, 1, 1}); // 179
    run_hunters_bound("x^7 + x^6 - 79*x^5 - 229*x^4 + 894*x^3 + 2844*x^2 - 2376*x - 7775", true, {-7775, -2376, 2844, 894, -229, -79, 1, 1}); // 159
    run_hunters_bound("x^7 + x^6 - 83*x^5 - 137*x^4 + 1594*x^3 + 2944*x^2 - 8352*x - 16127", true, {-16127, -8352, 2944, 1594, -137, -83, 1, 1}); // 167
    run_hunters_bound("x^7 + x^6 - 80*x^5 - 110*x^4 + 1849*x^3 + 2629*x^2 - 11850*x - 12599", true, {-12599, -11850, 2629, 1849, -110, -80, 1, 1}); // 161

    // a1 = 2, bound = 175.43489587910547854641656469828408033
    run_hunters_bound("x^7 + 2*x^6 - 86*x^5 - 388*x^4 + 265*x^3 + 2186*x^2 - 180*x - 1799", false, {-1799, -180, 2186, 265, -388, -86, 2, 1}); // 176
    run_hunters_bound("x^7 + 2*x^6 - 88*x^5 - 226*x^4 + 1811*x^3 + 5072*x^2 - 5900*x - 16799", false, {-16799, -5900, 5072, 1811, -226, -88, 2, 1}); // 180
    run_hunters_bound("x^7 + 2*x^6 - 86*x^5 - 208*x^4 + 2245*x^3 + 5966*x^2 - 18000*x - 50399", false, {-50399, -18000, 5966, 2245, -208, -86, 2, 1}); // 176
    run_hunters_bound("x^7 + 2*x^6 - 84*x^5 - 290*x^4 + 1079*x^3 + 3408*x^2 - 4356*x - 6479", true, {-6479, -4356, 3408, 1079, -290, -84, 2, 1}); // 172
    run_hunters_bound("x^7 + 2*x^6 - 79*x^5 - 208*x^4 + 1244*x^3 + 3488*x^2 - 3776*x - 10751", true, {-10751, -3776, 3488, 1244, -208, -79, 2, 1}); // 162
    run_hunters_bound("x^7 + 2*x^6 - 80*x^5 - 190*x^4 + 1819*x^3 + 4508*x^2 - 11100*x - 25199", true, {-25199, -11100, 4508, 1819, -190, -80, 2, 1}); // 164

    // a1 = 3, bound = 176.14918159339119283213085041256979461
    run_hunters_bound("x^7 + 3*x^6 - 85*x^5 - 475*x^4 - 36*x^3 + 2752*x^2 + 1440*x - 3599", false, {-3599, 1440, 2752, -36, -475, -85, 3, 1}); // 179
    run_hunters_bound("x^7 + 3*x^6 - 84*x^5 - 306*x^4 + 1425*x^3 + 6111*x^2 + 1250*x - 8399", false, {-8399, 1250, 6111, 1425, -306, -84, 3, 1}); // 177
    run_hunters_bound("x^7 + 3*x^6 - 86*x^5 - 258*x^4 + 1849*x^3 + 5547*x^2 - 1764*x - 5291", false, {-5291, -1764, 5547, 1849, -258, -86, 3, 1}); // 181
    run_hunters_bound("x^7 + 3*x^6 - 83*x^5 - 375*x^4 + 874*x^3 + 4692*x^2 - 2232*x - 12959", true, {-12959, -2232, 4692, 874, -375, -83, 3, 1}); // 175
    run_hunters_bound("x^7 + 3*x^6 - 83*x^5 - 275*x^4 + 1474*x^3 + 4192*x^2 - 6432*x - 8959", true, {-8959, -6432, 4192, 1474, -275, -83, 3, 1}); // 175
    run_hunters_bound("x^7 + 3*x^6 - 83*x^5 - 303*x^4 + 1810*x^3 + 7860*x^2 - 4248*x - 30239", true, {-30239, -4248, 7860, 1810, -303, -83, 3, 1}); // 175

    tests_teardown();

    log_info("End suite: Hunters Bound");
}
