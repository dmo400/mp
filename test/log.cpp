#include <iostream>
#include <string>
#include "log.hpp"

void log_info(std::string info) {
    std::cout << info << std::endl;
}

void log_pass(std::string message) {
    std::cout << "PASS: " << message << std::endl;
}

void log_fail(std::string message) {
    std::cout << "FAIL: " << message << std::endl;
}

void assert_bool(std::string test, bool expected, bool result) {
    if (result == expected) {
        log_pass(test);
    } else {
        if (result) {
            log_fail(test + " [expected false, got true]");
        } else {
            log_fail(test + " [expected true, got false]");
        }
    }
}

void assert_true(std::string test, bool result) {
    assert_bool(test, true, result);
}

void assert_false(std::string test, bool result) {
    assert_bool(test, false, result);
}

void assert_long(std::string test, long expected, long result) {
    if (result == expected) {
        log_pass(test);
    } else {
        log_fail(test + "[expected " + std::to_string(expected) + ", got " + std::to_string(result) + "]");
    }
}
