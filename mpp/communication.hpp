#ifndef COMMUNICATION_HPP
#define COMMUNCATION_HPP

#include <vector>
#include "mpi.h"

void submit_results_to(std::vector<std::vector<long>> &results, int rank, int tag);
void collect_statistics_to(int mpiRank, MPI_Comm collectComm, int collectTag);

#endif // COMMUNICATION_HPP
