#include "communication.hpp"
#include "statistics.hpp"

// Send every result in results to rank and empties results
void submit_results_to(std::vector<std::vector<long>> &results, int rank, int tag) {
    for (auto result : results) {
        MPI_Send(&result.front(), result.size(), MPI_LONG, rank, tag, MPI_COMM_WORLD);
    }
    results.clear();
}

// Collect average durations and total counts from everyone in collectComm to mpiRank in MPI_WORLD_COMM
// Will override ST_DURATIONS and ST_COUNTS of mpiRank
void collect_statistics_to(int mpiRank, MPI_Comm collectComm, int collectTag) {
    long recvDurations[ST_SIZE];
    long recvCounts[ST_SIZE];
    std::chrono::high_resolution_clock::duration clockDurations[ST_SIZE];
    long durations[ST_SIZE];
    long counts[ST_SIZE];
    int worldRank, collectRank, intermediateRank;
    MPI_Status status;
    
    MPI_Comm_rank(MPI_COMM_WORLD, &worldRank);
    
    if (worldRank == mpiRank) {
        // Receive average durations and total counts
        MPI_Recv(&recvDurations, ST_SIZE, MPI_DOUBLE, MPI_ANY_SOURCE, collectTag, MPI_COMM_WORLD, &status);
        MPI_Recv(&recvCounts, ST_SIZE, MPI_LONG, MPI_ANY_SOURCE, collectTag, MPI_COMM_WORLD, &status);
        for (int i = 0; i < ST_SIZE; i++) {
            clockDurations[i] = std::chrono::duration_cast<std::chrono::high_resolution_clock::duration>(std::chrono::microseconds(recvDurations[i]));
        }
        
        st_durations_reset_to(clockDurations);
        st_counts_reset_to(recvCounts);
    } else {
        MPI_Comm_rank(collectComm, &collectRank);
        MPI_Allreduce(&collectRank, &intermediateRank, 1, MPI_INT, MPI_MIN, collectComm);
        
        for (int i = 0; i < ST_SIZE; i++) {
            durations[i] = st_microseconds_elapsed(i);
            counts[i] = st_counted(i);
        }
        
        // Reduce average durations and total counts to the intermediateRank
        MPI_Reduce(&durations, &recvDurations, ST_SIZE, MPI_DOUBLE, MPI_SUM, intermediateRank, collectComm);
        MPI_Reduce(&counts, &recvCounts, ST_SIZE, MPI_LONG, MPI_SUM, intermediateRank, collectComm);
        
        // Send the collected statistics from intermediateRank to mpiRank in MPI_COMM_WORLD
        if (collectRank == intermediateRank) {
            MPI_Send(&recvDurations, ST_SIZE, MPI_DOUBLE, mpiRank, collectTag, MPI_COMM_WORLD);
            MPI_Send(&recvCounts, ST_SIZE, MPI_LONG, mpiRank, collectTag, MPI_COMM_WORLD);
        }
    }
}
