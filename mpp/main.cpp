#include <chrono>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <boost/thread.hpp>
#include "mpi.h"
#include "pari/pari.h"
#include "communication.hpp"
#include "polgenerator.hpp"
#include "statistics.hpp"
#include "poltests.hpp"
#include "workqueue.hpp"

/* PARI necessary global variables */
static long const PARI_STACK_SIZE = 1000000L;
static long const PARI_MAXPRIME = 2L;

/* MPI variables */
static int const WORK_GENERATOR_TAG = 0;
static int const WORKER_REQUEST_TAG = 1;
static int const RESULT_COLLECT_TAG = 2;
static int const STATS_COLLECT_TAG = 3;
static int const TERMINATION_TAG = 4;

int mpiRoot = 0;
int mpiRank, worldSize;

long genNum = -1;
int genVars = 4;
int resultVars = POL_TERMS + PATTERN_SIZE;
std::string filename = "results.txt";

// Write settings with which the computation is launched
// Opens and closes result file
void write_start_info() {
    std::ofstream file;
    file.open(filename, std::ios::app);
    if (file.is_open()) {
        std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
        std::time_t startTime = std::chrono::system_clock::to_time_t(start);
        file << "Program launched at " << std::ctime(&startTime);
        file << "Computation started with " << worldSize - 1 << " workers" << std::endl;
        file << "Generator set for " << genVars << " coefficients with limit " << genNum << std::endl;
    } else {
        std::cout << "could not open file " << filename << " for writing" << std::endl;
    }
    file.close();
}

// Write the result array in the format [coefficients] | nfdisc signature |
// Opens and closes result file
void write_result(long result[]) {
    std::ofstream file;
    file.open(filename, std::ios::app);
    if (file.is_open()) {
        file << "[";
        for (int i = 0; i < POL_DEGREE; i++) {
            file << result[i] << ", ";
        }
        file << result[POL_DEGREE] << "] | ";
        for (int i = POL_TERMS; i < resultVars; i++) {
            file << result[i] << " ";
        }
        file << "|" << std::endl;
    } else {
        std::cout << "could not open file " << filename << " for writing" << std::endl;
    }
    file.close();
}

// Read results from a file and write the reduced form at the end
// Uses a set to filter duplicates
void write_results_reduced() {
    std::fstream file;
    pari_sp ltop;
    GEN polynomial;
    std::set<std::vector<long>> reducedResults;
    long coefficients[POL_TERMS];
    long signature[PATTERN_SIZE];

    file.open(filename, std::ios::in);
    if (file.is_open()) {
        for (std::string line; std::getline(file, line);) {
            if (line[0] == '[') {
                std::istringstream input(line);
                input.ignore(line.length(), '[');
                for (int i = 0; i < POL_TERMS; i++) {
                    input >> coefficients[i];
                    input.ignore(1, ',');
                }
                input.ignore(1, ']');
                input.ignore(1, '|');
                for (int i = 0; i < PATTERN_SIZE; i++) {
                    input >> signature[i];
                    input.ignore(1, ',');
                }
                ltop = avma;
                polynomial = create_polynomial(coefficients);
                polynomial = polredbest(polynomial, 0);
                std::vector<long> reducedResult;
                for (int i = 0; i < POL_TERMS; i++) {
                    reducedResult.push_back(itos(gel(polynomial, 2 + i)));
                }
                for (int i = 0; i < PATTERN_SIZE; i++) {
                    reducedResult.push_back(signature[i]);
                }
                reducedResults.insert(reducedResult);
                avma = ltop;
            }
        }
    } else {
        std::cout << "could not open file " << filename << " for reading" << std::endl;
    }
    file.close();
    for (auto result : reducedResults) {
        write_result(result.data());
    }
}

// Helper for write_server and write_workers, writes 1 statistics line
// Requires open stream, does not close it
void write_stat(std::ostream &file, std::string name, int part) {
    long time = st_microseconds_elapsed(part);
    long total = st_counted(part);

    file << name << "\t" << total << "\t" << time;
    if (total > 0) {
        file << "\t" << time / total;
    } else {
        file << "\tN/A";
    }
    file << std::endl;
}

// Write total elapsed time and statistics, opens and closes result file
void write_workers() {
    std::ofstream file;
    file.open(filename, std::ios::app);
    if (file.is_open()) {
        file << "Workers\tAmount\tTotal Time (us)\tApprox Avg Time (us)" << std::endl;
        write_stat(file, "Workers:", ST_TOTAL);
        write_stat(file, "Receive:", ST_RECV);
        write_stat(file, "Generate:", ST_GENERATE);
        write_stat(file, "Retrieve:", ST_RETRIEVE);
        write_stat(file, "Runs:", ST_RUN);
        file << "Test\tAmount Tested\tTotal Time (us)\tApprox Avg Time (us)" << std::endl;
        write_stat(file, "Disc:", ST_DISC);
        write_stat(file, "Irred:", ST_IRRED);
        write_stat(file, "Hunter:", ST_HUNTER);
        write_stat(file, "Nf-disc:", ST_NFDISC);
        write_stat(file, "Saved:", ST_SAVE);
    } else {
        std::cout << "could not open file " << filename << " for writing" << std::endl;
    }
    file.close();
}

// Write server elapsed time and statistics, opens and closes result file
void write_server() {
    std::ofstream file;
    file.open(filename, std::ios::app);
    if (file.is_open()) {
        file << "Total elapsed time: " << st_microseconds_elapsed(ST_TOTAL) << "us" << std::endl;
        file << "Server\tAmount\tTotal Time (us)\tApprox Avg Time (us)" << std::endl;
        write_stat(file, "Receive:", ST_RECV);
        write_stat(file, "Generate:", ST_GENERATE);
        write_stat(file, "Store:", ST_STORE);
        write_stat(file, "Retrieve:", ST_RETRIEVE);
    } else {
        std::cout << "could not open file " << filename << " for writing" << std::endl;
    }
}

/*
 * Main loop generating and testing all polynomials
 */
void worker() {
    pari_sp ltop;
    PolGenerator polGen(BOUND_PART, POL_DEGREE, POL_DEGREE);
    long coefficients[8] = {0, 0, 0, 0, 0, 0, 0, 1};
    long *recvBuffer = new long[genVars];
    bool polAvailable = true;
    MPI_Status status;

    // Lock the generator to the number of coefficients root will send
    polGen.lock(genVars);
    while (true) {
        // Request root for work
        MPI_Send(nullptr, 0, MPI_LONG, mpiRoot, WORKER_REQUEST_TAG, MPI_COMM_WORLD);
        // Receive either work or a termination signal from root
        st_timer_record_start(ST_RECV); // Receive time starting point
        MPI_Recv(recvBuffer, genVars, MPI_LONG, mpiRoot, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        st_timer_store_elapsed(ST_RECV); // Receive time stopping point
        if (status.MPI_TAG == TERMINATION_TAG) {
            break;
        }
        // Received work so reset the generator to the received coefficients
        st_timer_record_start(ST_GENERATE); // Record generator initialize time
        polGen.load(recvBuffer, genVars);
        st_timer_store_elapsed(ST_GENERATE); // Record generator initialize time
        polAvailable = true;
        ltop = avma;
        // Test every polynomial generated with the given coefficients
        while (polAvailable) {
            st_timer_record_start(ST_RETRIEVE); // Retrieve time starting point
            polGen.create_coefficients(coefficients);
            st_timer_store_elapsed(ST_RETRIEVE); // Retrieve time stopping point
            test_polynomial(coefficients);
            st_timer_record_start(ST_GENERATE); // Generator time starting point
            polAvailable = polGen.configure_next();
            st_timer_store_elapsed(ST_GENERATE); // Generator time stopping point; note that the last call will fail, but still be timed and counted
            avma = ltop; // Clean up PARI stack space used to create the polynomial for testing
        }
        submit_results_to(testResults, mpiRoot, RESULT_COLLECT_TAG);
    }

    delete[] recvBuffer;
}

void request_handler(WorkQueue *storage) {
    long *coefficients = new long[genVars];
    long *result = new long[resultVars];
    long terminated = 1L; // Consider root as terminated from the start
    bool finished;
    MPI_Status status;
    
    st_timer_record_start(ST_RETRIEVE); // Retrieve time starting point
    finished = !storage->retrieve(coefficients); // Prepare the first workload
    st_timer_store_elapsed(ST_RETRIEVE); // Retrieve time stopping point
    // While work is available, distribute it
    while (!finished) {
        // Wait for work requests or a found result from any worker
        st_timer_record_start(ST_RECV); // Receive time starting point
        MPI_Recv(result, resultVars, MPI_LONG, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        st_timer_store_elapsed(ST_RECV); // Receive time stopping point
        if (status.MPI_TAG == WORKER_REQUEST_TAG) {
            // Received a work request, respond by sending work retrieved from the workqueue
            MPI_Send(coefficients, genVars, MPI_LONG, status.MPI_SOURCE, WORK_GENERATOR_TAG, MPI_COMM_WORLD);
            st_timer_record_start(ST_RETRIEVE); // Retrieve time starting point
            finished = !storage->retrieve(coefficients); // keep track if any work is still available/expected to come
            st_timer_store_elapsed(ST_RETRIEVE); // Retrieve time stopping point
        } else if (status.MPI_TAG == RESULT_COLLECT_TAG) {
            // Save the result from the worker
            write_result(result);
        }
    }
    // No more work available, collect results only and terminate every worker
    while (terminated != worldSize) {
        st_timer_record_start(ST_RECV); // Receive time starting point
        MPI_Recv(result, resultVars, MPI_LONG, MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        st_timer_store_elapsed(ST_RECV); // Receive time stopping point
        if (status.MPI_TAG == WORKER_REQUEST_TAG) {
            // Respond requests for new work with termination
            MPI_Send(nullptr, 0, MPI_LONG, status.MPI_SOURCE, TERMINATION_TAG, MPI_COMM_WORLD);
            terminated += 1; // track number of workers terminated
        } else if (status.MPI_TAG == RESULT_COLLECT_TAG) {
            // Save the last results from the worker
            write_result(result);
        }
    }
    
    delete[] coefficients;
    delete[] result;
}

void generate_coefficients(WorkQueue *storage) {
    PolGenerator polGen(BOUND_PART, POL_DEGREE, genVars);
    bool upped = true;
    long count = 0L;
    
    if (genNum == 0L) {
        storage->close();
        return;
    }
    st_timer_record_start(ST_GENERATE); // Record generator initialize time
    polGen.init();
    st_timer_store_elapsed(ST_GENERATE); // Record generator initialize time
    
    while (upped && (genNum < 0L || count < genNum)) {
        st_timer_record_start(ST_STORE); // Storage time starting point
        storage->submit(&polGen);
        st_timer_store_elapsed(ST_STORE); // Storage time stopping point
        count += 1L;
        st_timer_record_start(ST_GENERATE); // Generator time starting point
        upped = polGen.configure_next();
        st_timer_store_elapsed(ST_GENERATE); // Generator time stopping point; note that the last call will fail, but still be timed and counted
    }
    storage->close();
}

void init_arg(int argc, char **argv) {
    for (int i = 1; i < argc; i++) {
        int ni = i + 1;
        std::string cmdarg = argv[i];
        if (cmdarg == "-gn" || cmdarg == "-gennum") {
            // Control when generator stops
            if (ni >= argc) {
                std::cout << "-gn|-gennum requires a number to set num of jobs created or a negative number for no limit" << std::endl;
                std::exit(1);
            }
            std::istringstream iss(argv[ni]);
            if (!(iss >> genNum)) {
                std::cout << "-gn|-gennum requires a number to set num of jobs created or a negative number for no limit" << std::endl;
                std::exit(1);
            }
            i = ni;
        } else if (cmdarg == "-gv" || cmdarg == "-genvars") {
            // Control number of coefficients considered by the generator
            if (ni >= argc) {
                std::cout << "-gv|-genvars requires a number greater than 0 to set the amount of vars created by the generator" << std::endl;
                std::exit(1);
            }
            std::istringstream iss(argv[ni]);
            if (!(iss >> genVars) || (genVars < 1)) {
                std::cout << "-gv|-genvars requires a number greater than 0 to set the amount of vars created by the generator" << std::endl;
                std::exit(1);
            }
            i = ni;
        } else if (cmdarg == "-o" || cmdarg == "-output") {
            // Specify name of result file
            if (ni >= argc) {
                std::cout << "-o|-output requires a name for the file to store the results in" << std::endl;
                std::exit(1);
            }
            filename= argv[ni];
            i = ni;
        } else {
            // Invalid commandline argument given, report and exit
            std::cout << "Unknown argument " << argv[i] << std::endl;
            exit(1);
        }
    }
}

void collect_statistics() {
    MPI_Comm workersComm;
    MPI_Group worldGroup, workersGroup;
    const int exclRoot[1] = {mpiRoot};
    
    MPI_Comm_group(MPI_COMM_WORLD, &worldGroup);
    MPI_Group_excl(worldGroup, 1, exclRoot, &workersGroup);
    MPI_Comm_create_group(MPI_COMM_WORLD, workersGroup, 0, &workersComm);
    collect_statistics_to(mpiRoot, workersComm, STATS_COLLECT_TAG);
}

int main(int argc, char **argv) {
    init_arg(argc, argv);
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpiRank);
    MPI_Comm_size(MPI_COMM_WORLD, &worldSize);
    
    if (worldSize < 2) {
        std::cout << "Program requires at least 2 nodes, exiting" << std::endl;
        MPI_Finalize();
        return 0;
    }
    
    pari_init(PARI_STACK_SIZE, PARI_MAXPRIME);
    st_init();
    
    if (mpiRank == mpiRoot) {
        write_start_info();
    }
    
    MPI_Barrier(MPI_COMM_WORLD);
    st_timer_record_start(ST_TOTAL); // Root and Workers total time starting point
    tests_setup();
    
    if (mpiRank == mpiRoot) {
        WorkQueue storage(1000, genVars);
        boost::thread handler(request_handler, &storage);
        generate_coefficients(&storage);
        handler.join();
    } else {
        worker();
        st_timer_store_elapsed(ST_TOTAL); // Workers only total time stopping point
    }
    
    MPI_Barrier(MPI_COMM_WORLD);
    if (mpiRank == mpiRoot) {
        st_timer_store_elapsed(ST_TOTAL); // Root only total time stopping point
    }
    
    if (mpiRank == mpiRoot) {
        write_server();
    }
    collect_statistics();
    if (mpiRank == mpiRoot) {
        write_workers();
        write_results_reduced();
    }
    
    tests_teardown();
    pari_close();
    MPI_Finalize();
    
    return 0;
}
