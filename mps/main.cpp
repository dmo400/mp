#include <chrono>
#include <ctime>
#include <fstream>
#include <iostream>
#include <set>
#include <sstream>
#include "pari/pari.h"
#include "polgenerator.hpp"
#include "statistics.hpp"
#include "poltests.hpp"

/* PARI necessary global variables */
static long const PARI_STACK_SIZE = 1000000L;
static long const PARI_MAXPRIME = 2L;

long genNum = -1;
int genVars = 4;
int resultVars = POL_TERMS + PATTERN_SIZE;
std::string filename = "results.txt";

// Write settings whith which the computation is launched
// Opens and closes result file
void write_start_info() {
    std::ofstream file;
    file.open(filename, std::ios::app);
    if (file.is_open()) {
        std::chrono::time_point<std::chrono::system_clock> start = std::chrono::system_clock::now();
        std::time_t startTime = std::chrono::system_clock::to_time_t(start);
        file << "Program launched at " << std::ctime(&startTime);
        file << "Computation started sequential version" << std::endl;
        file << "Generator set for " << genVars << " coefficients with limit " << genNum << std::endl;
    } else {
        std::cout << "could not open file " << filename << " for writing" << std::endl;
    }
    file.close();
}

// Write the result array in the format [coefficients] | nfdisc signature |
// Opens and closes result file
void write_results(const std::vector<std::vector<long>> &results) {
    std::ofstream file;
    file.open(filename, std::ios::app);
    if (file.is_open()) {
        for (const std::vector<long> &result : results) {
            file << "[";
            for (int i = 0; i < POL_DEGREE; i++) {
                file << result[i] << ", ";
            }
            file << result[POL_DEGREE] << "] | ";
            for (int i = POL_TERMS; i < resultVars; i++) {
                file << result[i] << " ";
            }
            file << "|" << std::endl;
        }
    } else {
        std::cout << "could not open file " << filename << " for writing" << std::endl;
    }
    file.close();
}

// Read results from a file and write the reduced form at the end
// Uses a set to filter duplicates
void write_results_reduced() {
    std::fstream file;
    pari_sp ltop;
    GEN polynomial;
    std::set<std::vector<long>> reducedResults;
    long coefficients[POL_TERMS];
    long signature[PATTERN_SIZE];
    
    file.open(filename, std::ios::in);
    if (file.is_open()) {
        for (std::string line; std::getline(file, line);) {
            if (line[0] == '[') {
                std::istringstream input(line);
                input.ignore(line.length(), '[');
                for (int i = 0; i < POL_TERMS; i++) {
                    input >> coefficients[i];
                    input.ignore(1, ',');
                }
                input.ignore(1, ']');
                input.ignore(1, '|');
                for (int i = 0; i < PATTERN_SIZE; i++) {
                    input >> signature[i];
                    input.ignore(1, ',');
                }
                ltop = avma;
                polynomial = create_polynomial(coefficients);
                polynomial = polredbest(polynomial, 0);
                std::vector<long> reducedResult;
                for (int i = 0; i < POL_TERMS; i++) {
                    reducedResult.push_back(itos(gel(polynomial, 2 + i)));
                }
                for (int i = 0; i < PATTERN_SIZE; i++) {
                    reducedResult.push_back(signature[i]);
                }
                reducedResults.insert(reducedResult);
                avma = ltop;
            }
        }
    } else {
        std::cout << "could not open file " << filename << " for reading" << std::endl;
    }
    file.close();
    std::vector<std::vector<long>> finalResults(reducedResults.begin(), reducedResults.end());
    write_results(finalResults);
}

// Helper for write_end, writes 1 statistics line
// Requires open stream, does not close it
void write_stat(std::ostream &file, std::string name, int part) {
    long time = st_microseconds_elapsed(part);
    long total = st_counted(part);
    
    file << name << "\t" << total << "\t" << time;
    if (total > 0) {
        file << "\t" << time / total;
    } else {
        file << "\tN/A";
    }
    file << std::endl;
}

// Write statistics, opens and closes result file
void write_end() {
    std::ofstream file;
    file.open(filename, std::ios::app);
    if (file.is_open()) {
        file << "Total elapsed time: " << st_microseconds_elapsed(ST_TOTAL) << "us" << std::endl;
        file << "Overview\tAmount\tTotal Time (us)\tApprox Avg Time (us)" << std::endl;
        write_stat(file, "Generate:", ST_GENERATE);
        write_stat(file, "Retrieve:", ST_RETRIEVE);
        write_stat(file, "Runs:", ST_RUN);
        file << "Test\tAmount Tested\tTotal Time (us)\tApprox Avg Time (us)" << std::endl;
        write_stat(file, "Disc:", ST_DISC);
        write_stat(file, "Irred:", ST_IRRED);
        write_stat(file, "Hunter:", ST_HUNTER);
        write_stat(file, "Nf-disc:", ST_NFDISC);
        write_stat(file, "Saved:", ST_SAVE);
    } else {
        std::cout << "could not open file " << filename << " for writing" << std::endl;
    }
    file.close();
}

/*
 * Main loop generating and testing all polynomials
 */
void do_work() {
    pari_sp ltop;
    PolGenerator polGen(BOUND_PART, POL_DEGREE);
    long coefficients[8] = {0, 0, 0, 0, 0, 0, 0, 1};
    bool polAvailable = true;
    long count = 0;

    if (genNum == 0L) {
        return;
    }

    st_timer_record_start(ST_GENERATE); // Record generator initialize time
    polGen.init();
    st_timer_store_elapsed(ST_GENERATE); // Record generator initialize time

    // Generation split into countable workloads by having 2 loops
    while (polAvailable && (genNum < 0L || count < genNum)) {
        polGen.lock(genVars);
        polAvailable = true;
        ltop = avma;
        // Test every polynomial generated with the given coefficients, single workload loop
        while (polAvailable) {
            st_timer_record_start(ST_RETRIEVE); // Retrieve time starting point
            polGen.create_coefficients(coefficients);
            st_timer_store_elapsed(ST_RETRIEVE); // Retrieve time stopping point
            test_polynomial(coefficients);
            st_timer_record_start(ST_GENERATE); // Generator time starting point
            polAvailable = polGen.configure_next();
            st_timer_store_elapsed(ST_GENERATE); // Generator time stopping point; note that the last call will fail, but still be timed and counted
            avma = ltop; // Clean up PARI stack space used to create the polynomial for testing
        }
        count += 1L;
        polGen.lock(0);
        // Prepare next workload
        st_timer_record_start(ST_GENERATE); // Generator time starting point
        polAvailable = polGen.configure_next();
        st_timer_store_elapsed(ST_GENERATE); // Generator time stopping point; note that the last call will fail, but still be timed and counted
    }
}

void init_arg(int argc, char **argv) {
    for (int i = 1; i < argc; i++) {
        int ni = i + 1;
        std::string cmdarg = argv[i];
        if (cmdarg == "-gn" || cmdarg == "-gennum") {
            // Control when generator stops
            if (ni >= argc) {
                std::cout << "-gn|-gennum requires a number to set num of jobs created or a negative number for no limit" << std::endl;
                std::exit(1);
            }
            std::istringstream iss(argv[ni]);
            if (!(iss >> genNum)) {
                std::cout << "-gn|-gennum requires a number to set num of jobs created or a negative number for no limit" << std::endl;
                std::exit(1);
            }
            i = ni;
        } else if (cmdarg == "-gv" || cmdarg == "-genvars") {
            // Control the coefficient which will count as 1 job when upped
            if (ni >= argc) {
                std::cout << "-gv|-genvars requires a number greater than 0 to set the coefficient which will count for gennum as 1 job" << std::endl;
                std::exit(1);
            }
            std::istringstream iss(argv[ni]);
            if (!(iss >> genVars) || (genVars < 1)) {
                std::cout << "-gv|-genvars requires a number greater than 0 to set the coefficient which will count for gennum as 1 job" << std::endl;
                std::exit(1);
            }
            i = ni;
        } else if (cmdarg == "-o" || cmdarg == "-output") {
            // Specify name of result file
            if (ni >= argc) {
                std::cout << "-o|-output requires a name for the file to store the results in" << std::endl;
                std::exit(1);
            }
            filename= argv[ni];
            i = ni;
        } else {
            // Invalid commandline argument given, report and exit
            std::cout << "Unknown argument " << argv[i] << std::endl;
            exit(1);
        }
    }
}

int main(int argc, char **argv) {
    init_arg(argc, argv);
    
    pari_init(PARI_STACK_SIZE, PARI_MAXPRIME);
    st_init();
    
    write_start_info();
    
    st_timer_record_start(ST_TOTAL);
    tests_setup();
    
    do_work();
    write_results(testResults);
    
    st_timer_store_elapsed(ST_TOTAL);
    
    write_end();
    write_results_reduced();
    
    tests_teardown();
    pari_close();
    
    return 0;
}
